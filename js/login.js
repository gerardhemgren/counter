const form = document.getElementById('login-form');
const alertMessage = document.getElementById('alert');

const users = [
    { email: "apagar11@gmail.com", password: "test1" },
    { email: "q@", password: "q" }
];

function validateForm() {
    if (!form.email.value.includes('@')) {
        alertMessage.innerHTML = 'Enter a valid e-mail';
    } else if (!form.password.value) {
        alertMessage.innerHTML = 'Please enter a password';
    } else {
        const validUser = users.find(u => u.email === form.email.value && u.password === form.password.value);
        if (!validUser) {
            alertMessage.innerHTML = 'Invalid e-mail or password';
        }
        else {
            alertMessage.innerHTML = '';
            localStorage.setItem('user', form.email.value);
            location = "/index.html";
        }
    }
}