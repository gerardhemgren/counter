const resultElement = document.getElementById('result');
const clicksDates = JSON.parse(localStorage.getItem('dates')) || [];
const btnUndoElement = document.getElementById('btn-undo');
const showDate = document.getElementById('date');

resultElement.innerHTML = clicksPerDay().toString();

moment.locale('es');
function showLastClick() {
    showDate.innerHTML = clicksPerDay() ? moment(clicksDates[clicksLength() - 1]).format('HH:mm:ss<br>dddd D') : '';
}

// Sets clicks on LS and then prints it in id:result
function clickCounter(element, addition) {
    element.addEventListener('click', function () {
        if (addition) {
            clicksDates.push(new Date());
        } else if (clicksPerDay()) {
            clicksDates.pop();
        }
        localStorage.setItem('dates', JSON.stringify(clicksDates));
        resultElement.innerHTML = clicksPerDay().toString();
        showLastClick();
    });
}

clickCounter(resultElement, true);
clickCounter(btnUndoElement, false);
showLastClick();

function clicksLength() {
    return clicksDates.length;
}

function clicksPerDay() {
    return clicksDates.filter(dates => moment().isSame(dates, 'day')).length;
}

function logout() {
    localStorage.removeItem('user');
}

// Week Statistics
let daysToSubtract = 1;
function getSubtractedDay() {
    return moment().subtract(daysToSubtract, 'days')
}
for (; daysToSubtract < 8; daysToSubtract++) {
    const weekStatistics = document.getElementById('week-statistics');
    const amountPerDay = document.createElement('div');
    const day = document.createElement('div');
    amountPerDay.innerHTML = clicksDates.filter(date => getSubtractedDay().isSame(date, 'day')).length;
    amountPerDay.classList.add('amount-per-day');
    day.innerHTML = getSubtractedDay().format('ddd')
    day.classList.add('day');
    weekStatistics.appendChild(amountPerDay)
    amountPerDay.appendChild(day)
}